import { createRouter, createWebHistory } from 'vue-router'
import App from './App.vue'
import LogIn from './components/LogIn.vue'
import SignUp from './components/SignUp.vue'
import home from './components/Home.vue'
import schedule from './components/schedule.vue'
import Group from './components/group.vue'
import reservation from './components/reservation.vue'
import Account from './components/Account.vue'

const routes = [{
    path: '/',
    name: 'root',
    component: App
  },
  {
    path: '/user/logIn',
    name: 'logIn',
    component: LogIn
  },
  {
    path: '/user/signUp',
    name: 'signUp',
    component: SignUp
  },
  {path: '/user/home',
  name: 'home',
  component: home
},
{
  path: '/user/schedule',
  name: 'schedule',
  component: schedule
},
{
  path: '/user/reservation',
  name: 'reservation',
  component: reservation
},
{
  path: '/user/group',
  name: 'group',
  component: Group
},
{
  path: '/user/account',
  name: 'account',
  component: Account
}
]
const router = createRouter({
  history: createWebHistory(),
routes
})
export default router